
#ifndef __MONITOR_H__
#define __MONITOR_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "hwifi.h"
#include "hlog.h"
#include "hsocket_client.h"


void monitor_task(void * pvParameters);

#endif
