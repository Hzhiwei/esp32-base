
#include "monitor.h"

#define LOGTAG  "MONITOR"


void recv_cb(char *data, int len) {
    data[len] = '\0';
    HLOGI("cb-monitor", "%d: %s", len, data);
}

void monitor_task(void * pvParameters) {
    while(true) {
        vTaskDelay(1 / portTICK_PERIOD_MS);
    }
}

