#ifndef __HSOCKET_COMMON_H__
#define __HSOCKET_COMMON_H__

#include "hwifi.h"
#include "hlog.h"
 
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"


typedef struct {
    char ip[16];
    int port;
}hsocket_param;


#endif
