#ifndef __HSOCKET_CLIENT_H__
#define __HSOCKET_CLIENT_H__


#include "hsocket_common.h"


typedef struct {
    int sock;                           //socket
    hsocket_param param;                //socket连接目标ip port
    TaskHandle_t task;
    EventGroupHandle_t event;           //用于标志状态的事件组
    
    int rec_buffer_size;
    char *rec_buffer;                   //用于标志状态的事件组
    void (*cb)(char *data, int len);    //收到数据的回调函数
}hsocket_client_wrap;


hsocket_client_wrap *hsocket_client_warp_create(int rec_buffer_size, void (*cb)(char *data, int len));
void hsocket_client_warp_delete(hsocket_client_wrap *warp);
bool hsocket_client_is_connected(hsocket_client_wrap *warp);
bool hsocket_client_wait_connect(hsocket_client_wrap *warp, TickType_t timeout);
bool hsocket_client_wait_disconnect(hsocket_client_wrap *warp, TickType_t timeout);
void hsocket_client_connect(hsocket_client_wrap *warp);
void hsocket_client_disconnect(hsocket_client_wrap *warp);
bool hsocket_client_send(hsocket_client_wrap *warp, const uint8_t *data, int len, TickType_t timeout);


#endif
