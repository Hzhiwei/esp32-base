
#ifndef __HWIFI_H__
#define __HWIFI_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "hlog.h"
#include <string.h>

//状态机状态
typedef enum {
    hwifi_status_unknow,
    hwifi_status_unready,
    hwifi_status_idle,
    hwifi_status_connecting,
    hwifi_status_connected,
    hwifi_status_gotip,
    hwifi_status_disconnecting,
}hwifi_status_type;

//驱动wifi状态机的事件
typedef enum {
    hwifi_driver_unkonw,
    hwifi_driver_start,
    hwifi_driver_connect,
    hwifi_driver_gotip,
    hwifi_driver_lostip,
    hwifi_driver_disconnect,
    hwifi_driver_set
}hwifi_driver_type;

//状态机目标状态
typedef enum {
    hwifi_target_idle,
    hwifi_target_gotip,
    hwifi_target_reconnect,
}hwifi_target_type;

//用于连接的参数
typedef struct{
    uint8_t ssid[32];      /**< SSID of target AP. */
    uint8_t password[64];  /**< Password of target AP. */
}hwifi_connect_param_type;


void hwifi_init(void);
hwifi_status_type hwifi_get_status(void);
bool hwifi_wait_idle(TickType_t timeout);
bool hwifi_wait_connected(TickType_t timeout);
bool hwifi_wait_gotip(TickType_t timeout);
void hwifi_connect(const hwifi_connect_param_type *param);
void hwifi_reconnect(void);
void hwifi_disconnect(void);

#endif
