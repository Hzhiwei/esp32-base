
#ifndef __HNVS_H__
#define __HNVS_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_sleep.h"
#include "esp_flash.h"

#include "hwifi.h"
#include "hlog.h"

void hnvs_write_ssid(const char *ssid);
void hnvs_read_ssid(char *ssid);
void hnvs_write_password(const char *password);
void hnvs_read_password(char *password);
void hnvs_write_name(const char *name);
void hnvs_read_name(char *name);
void hnvs_write_url(const char *url);
void hnvs_read_url(char *url);
void hnvs_write_uuid(const char *uuid);
void hnvs_read_uuid(char *uuid);

#endif
