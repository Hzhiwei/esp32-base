
#ifndef __TEST_SOCKET_H__
#define __TEST_SOCKET_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "hwifi.h"
#include "hlog.h"
#include "hsocket_client.h"


void test_hsocket(void);

#endif
