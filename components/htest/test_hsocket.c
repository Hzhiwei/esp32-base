
#include "test_hsocket.h"
#include <string.h>

#define LOGTAG  "MONITOR"


static void recv_cb_1(char *data, int len);
static void recv_cb_2(char *data, int len);
void test_hsocket_sub1_task(void * pvParameters);
void test_hsocket_sub2_task(void * pvParameters);


void test_hsocket(void) {
    hwifi_connect_param_type param = {
        .ssid = "LANSI",
        .password = "Lansi1234"
    };
    HLOGI("hmonitor", "start hwifi_connect");
    hwifi_connect(&param);
    
    xTaskCreate(test_hsocket_sub1_task, "test_hsocket_sub1_task", 4096, NULL, tskIDLE_PRIORITY, NULL);
    xTaskCreate(test_hsocket_sub2_task, "test_hsocket_sub1_task", 4096, NULL, tskIDLE_PRIORITY, NULL);
}

void test_hsocket_sub1_task(void * pvParameters) {
    hsocket_client_wrap *warp;
    warp = hsocket_client_warp_create(512, recv_cb_1);

    strcpy(warp->param.ip, "192.168.0.194");
    warp->param.port = 8923;

    hsocket_client_connect(warp);
    HLOGI("test_hsocket", "test_hsocket_sub1_task start connect");
    while(false == hsocket_client_wait_connect(warp, 2000)) {
        HLOGI("test_hsocket", "test_hsocket_sub1_task wait connect");
    }
    HLOGI("test_hsocket", "test_hsocket_sub1_task connect success");

    while(true) {
        hsocket_client_send(warp, (const uint8_t *)"task1", 5, portMAX_DELAY);
        HLOGI("test_hsocket", "test_hsocket_sub1_task hsocket_client_send");
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

void test_hsocket_sub2_task(void * pvParameters) {
    hsocket_client_wrap *warp;
    warp = hsocket_client_warp_create(512, recv_cb_2);

    strcpy(warp->param.ip, "192.168.0.194");
    warp->param.port = 8924;

    hsocket_client_connect(warp);
    HLOGI("test_hsocket", "test_hsocket_sub2_task start connect");
    while(false == hsocket_client_wait_connect(warp, 2000)) {
        HLOGI("test_hsocket", "test_hsocket_sub2_task wait connect");
    }
    HLOGI("test_hsocket", "test_hsocket_sub2_task connect success");

    while(true) {
        hsocket_client_send(warp, (const uint8_t *)"task2", 5, portMAX_DELAY);
        HLOGI("test_hsocket", "test_hsocket_sub2_task hsocket_client_send");
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

static void recv_cb_1(char *data, int len) {
    data[len] = '\0';
    HLOGI("cb-monitor-1", "%d: %s", len, data);
}

static void recv_cb_2(char *data, int len) {
    data[len] = '\0';
    HLOGI("cb-monitor-2", "%d: %s", len, data);
}
