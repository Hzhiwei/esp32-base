
#include "esp_log.h"


#define HLOGE(tag, format, ...)  ESP_LOGE(tag, __FILE__ "/%d:" format, __LINE__, ##__VA_ARGS__)
#define HLOGW(tag, format, ...)  ESP_LOGW(tag, __FILE__ "/%d:" format, __LINE__, ##__VA_ARGS__)
#define HLOGI(tag, format, ...)  ESP_LOGI(tag, __FILE__ "/%d:" format, __LINE__, ##__VA_ARGS__)
#define HLOGD(tag, format, ...)  ESP_LOGD(tag, __FILE__ "/%d:" format, __LINE__, ##__VA_ARGS__)
#define HLOGV(tag, format, ...)  ESP_LOGV(tag, __FILE__ "/%d:" format, __LINE__, ##__VA_ARGS__)
