
#ifndef __HINIT_H__
#define __HINIT_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "hwifi.h"
#include "monitor.h"


bool system_init(void);


#endif
