
#include "hinit.h"
#include "test_hsocket.h"

bool system_init(void) {
    HLOGI("hinit", "init start");

    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    hwifi_init();

    test_hsocket();

    HLOGI("hinit", "init finished");
    return true;
}

